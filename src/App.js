import React, { Component } from "react";
//------------------------------------------------------------
import FormController from "./controller/FormController";
//============================================================

class App extends Component {
  render() {
    return (
      <div className="App container">
        <FormController />
      </div>
    );
  }
}

export default App;
