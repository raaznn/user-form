import React, { Component } from "react";
//=====================================================

export default class TableRow extends Component {
  render() {
    return (
      <tr>
        <th className="align-middle"> {this.props.index} </th>
        <td className="align-middle"> {this.props.userName} </td>
        <td className="align-middle"> {this.props.userMail}</td>
        <td className="align-middle"> {this.props.userPhone}</td>
        <td>
          <button
            className="btn"
            onClick={() => this.props.deleteFunc(this.props.index - 1)}
          >
            <i title="Delete Record" className="fas fa-trash text-danger"></i>
          </button>
        </td>
      </tr>
    );
  }
}
