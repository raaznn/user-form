import React, { Component } from "react";
//-----------------------------------------------------
import TableRow from "./TableRow";
//=====================================================

export default class FormController extends Component {
  state = {
    userName: "",
    userMail: "",
    userPhone: "",
    userData: [],
    errorMsg: "",
    successMsg: "",
  };

  //for clearInterval function
  successIntervalId = "";
  errorIntervalId = "";

  //get user input
  handleChange = (e) => {
    const newObj = {};
    newObj[e.target.name] = e.target.value;
    this.setState(newObj);
  };

  //save user input in userData
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.userName && this.state.userMail && this.state.userPhone) {
      this.setState({
        userData: [
          {
            userName: this.state.userName,
            userMail: this.state.userMail,
            userPhone: this.state.userPhone,
          },
          ...this.state.userData,
        ],
      });
      this.setState({
        userName: "",
        userPhone: "",
        userMail: "",
        successMsg: "Added Successfully",
      });
      //show added successfully message
      clearInterval(this.successIntervalId);
      this.successIntervalId = setTimeout(() => {
        this.setState({ successMsg: "" });
      }, 4000);
    } else {
      //show error if field is empty
      this.setState({ errorMsg: "All fields required" });
      clearInterval(this.errorIntervalId);
      this.errorIntervalId = setTimeout(() => {
        this.setState({ errorMsg: "" });
      }, 4000);
    }
  };

  //delete field from table
  deleteUser = (id) => {
    let newData = [...this.state.userData];
    newData.splice(id, 1);
    this.setState({ userData: newData, errorMsg: "Deleted Successfully" });
    clearInterval(this.errorIntervalId);
    this.errorIntervalId = setTimeout(() => {
      this.setState({ errorMsg: "" });
    }, 4000);
  };

  render() {
    return (
      <div className="container m-4">
        <h1 className="text-center"> User Information Form </h1>
        {/* user input form  */}
        <form
          style={{ maxWidth: "500px" }}
          className="mx-auto border p-5 my-5 rounded"
          onSubmit={this.handleSubmit}
        >
          <div className="form-group p-2">
            <label className="form-label">UserName</label>
            <input
              className="form-control"
              type="text"
              name="userName"
              value={this.state.userName}
              onChange={this.handleChange}
              autoComplete="off"
            ></input>
          </div>
          <div className="form-group p-2">
            <label className="form-label">Email</label>
            <input
              className="form-control"
              type="email"
              name="userMail"
              value={this.state.userMail}
              onChange={this.handleChange}
            ></input>
          </div>
          <div className="form-group p-2">
            <label className="form-label">Phone</label>
            <input
              className="form-control"
              type="number"
              name="userPhone"
              value={this.state.userPhone}
              onChange={this.handleChange}
            ></input>
          </div>
          <div className="form-group p-2 text-center">
            <button className="btn btn-primary w-50 mt-4" type="submit">
              SUBMIT{" "}
            </button>
          </div>
        </form>
        {/* user data table (if data exist)  */}
        {this.state.userData.length ? (
          <div style={{ height: "100vh" }} className="container ">
            <h1 className="text-center mt-5 mb-5"> User Information Table </h1>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th> # </th>
                  <th> Username </th>
                  <th> Email Address</th>
                  <th> Phone Number </th>
                  <th> Action </th>
                </tr>
              </thead>
              <tbody>
                {this.state.userData.map((data, ind) => {
                  return (
                    <TableRow
                      key={ind}
                      index={ind + 1}
                      {...data}
                      deleteFunc={this.deleteUser}
                    />
                  );
                })}
              </tbody>
            </table>
          </div>
        ) : (
          ""
        )}
        {/* Success / error Messages  */}
        {this.state.successMsg && (
          <h3 className="fixed-bottom text-light bg-success p-3 px-5 m-3 rounded w-50 ">
            {this.state.successMsg}
          </h3>
        )}
        {this.state.errorMsg && (
          <h3 className="fixed-bottom text-light bg-danger p-3 px-5 m-3 rounded w-50 ">
            {this.state.errorMsg}
          </h3>
        )}{" "}
      </div>
    );
  }
}
